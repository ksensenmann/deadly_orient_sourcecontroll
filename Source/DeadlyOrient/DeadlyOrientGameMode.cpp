// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DeadlyOrientGameMode.h"
#include "DeadlyOrientPlayerController.h"
#include "DeadlyOrientCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADeadlyOrientGameMode::ADeadlyOrientGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADeadlyOrientPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
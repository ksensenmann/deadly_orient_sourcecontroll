// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DeadlyOrient.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DeadlyOrient, "DeadlyOrient" );

DEFINE_LOG_CATEGORY(LogDeadlyOrient)
 